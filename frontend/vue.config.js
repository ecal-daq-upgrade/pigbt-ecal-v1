// vue.config.js
module.exports = {
    devServer: {
        host: '0.0.0.0',
        port: '8082',
        proxy: 'http://localhost:5002'
    },
    chainWebpack: config => {
        config.module.rules.delete('eslint');
    },
}
