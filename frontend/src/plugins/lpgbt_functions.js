import axios from "axios";

var lpgbt_lib = {

  async check_existence_lpgbts () {
    return new Promise((resolve, reject) => { 
      axios.get('/api/check_lpgbts_existence').then((response) => {
        if (response.data.error !== "") {
          reject(response.data.error)
        }
        resolve(response.data.status)
      })
    })
  },
  async get_slaves_addr(master_addr){
    return new Promise((resolve, reject) => {
	axios.post('/api/get_slaves_addr', {"master_addr":master_addr}).then((response) => {
	console.log(response)
        if (response.data.result.error !== "") {
          reject(response.data.result.error)
        }
        resolve(response.data.result.status)
      })
    })
  },
  async get_chips_id() {
    return new Promise((resolve, reject) => {
      axios.get('/api/get_chips_id').then((response) => {
        if (response.data.error !== "") {
          reject(response.data.error)
        }
        resolve(response.data.status)
      })
    })
  },

  async configure_all_lpgbts(lpgbt_list) {
    return new Promise((resolve,reject) => {
	axios.post('/api/config_all_lpgbts',{'lpgbt_list':lpgbt_list}).then((response) => {
	console.log(response)
        if (response.data.result.error !== "") {
          reject(response.data.result.error)
        }
        resolve(response.data.result.status)
      })
    })
  },

  async get_lpgbts_temperature() {
    return new Promise((resolve,reject) => {
      axios.get('/api/get_lpgbt_temps').then((response) => {
        if (response.data.error !== "") {
          reject(response.data.error)
        }
        resolve(response.data.status)
      })
    })
  },

  async get_tclink_status() {
    return new Promise((resolve,reject) => {
      axios.get('/api/get_tclink_status').then((response) => {
        if (response.data.error !== "") {
          reject(response.data.error)
        }
        resolve(response.data.status)
      })
    })
  }, 
 
  async get_sca_status() {
    return new Promise((resolve,reject) => {
      axios.get('/api/get_sca_status').then((response) => {
        if (response.data.error !== "") {
          reject(response.data.error)
        }
        resolve(response.data.status)
      })
    })
  },
 
  async align_BCP_sync_mode() {
    return new Promise((resolve,reject) => {
      axios.get('/api/align_BCP_sync').then((response) => {
        if (response.data.error !== "") {
          reject(response.data.error)
        } 
        resolve(response.data.status)
      })
    })
  },

  async optimize_pedestals(calib_ADCs) {
    return new Promise((resolve,reject) => {
      let option = {calib_ADCs : calib_ADCs}
      axios.post('/api/optimize_pedestals', option).then((response) => {
        if (response.data.error !== "") {
          reject(response.data.error)
        }
        resolve(response.data.status)
      })
    })
  },

  async find_VFE_devices() {
    return new Promise((resolve,reject) => {
      axios.get('/api/find_VFE_devices').then((response) => {
        if (response.data.error !== "") {
          reject(response.data.error)
        }
        resolve(response.data.status)
      })
    })
  },
}

export default lpgbt_lib
