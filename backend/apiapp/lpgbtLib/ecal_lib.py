import time

REG2STR_DTU = {
    0x00: "MAIN CONTROL",
    0x01: "ADC CONTROL",
    0x02: "CLPS DRIVERS",
    0x03: "CLPS PRE_EN",
    0x04: "CLPS RECEIVERS",
    0x05: "BASELINE H",
    0x06: "BASELINE L",
    0x07: "PLL BIAS",
    0x08: "PLL BIAS 1",
    0x09: "PLL BIAS 2",
    0x0a: "PLL BIAS 3",
    0x0b: "PLL BIAS 4",
    0x0c: "PLL BIAS 5",
    0x0d: "PLL CONTROL",
    0x0e: "PLL CONTROL 1",
    0x0f: "PLL CONTROL 2",
    0x10: "PLL CONTROL 3",
    0x11: "BASE SUBTRACTION 1",
    0x12: "BASE SUBTRACTION 2",
    0x13: "STATUS",
}

REG2STR_CATIA = {
    0x00: "NAME_1",
    0x01: "NAME_2",
    0x02: "NAME_3",
    0x03: "NAME_4",
    0x04: "NAME_5",
    0x05: "NAME_6",
    0x06: "NAME_7",
    0x07: "NAME_8",
    0x08: "NAME_9",
}

vtrx_reg_map = {}
vtrx_reg_map[0x0] = {"desc": "Global Control", "val":0};
vtrx_reg_map[0x2] = {"desc": "SDA Control", "val":0};
vtrx_reg_map[0x3] = {"desc": "Channel 1 Bias Current", "val":0};
vtrx_reg_map[0x6] = {"desc": "Channel 2 Bias Current", "val":0};
vtrx_reg_map[0x9] = {"desc": "Channel 3 Bias Current", "val":0};
vtrx_reg_map[0xC] = {"desc": "Channel 4 Bias Current", "val":0};
vtrx_reg_map[0x4] = {"desc": "Channel 1 Modulation Current", "val":0};
vtrx_reg_map[0x7] = {"desc": "Channel 2 Modulation Current", "val":0};
vtrx_reg_map[0xA] = {"desc": "Channel 3 Modulation Current", "val":0};
vtrx_reg_map[0xD] = {"desc": "Channel 4 Modulation Current", "val":0};
vtrx_reg_map[0x5] = {"desc": "Channel 1 Emphasis Amplitude", "val":0};
vtrx_reg_map[0x8] = {"desc": "Channel 2 Emphasis Amplitude", "val":0};
vtrx_reg_map[0xB] = {"desc": "Channel 3 Emphasis Amplitude", "val":0};
vtrx_reg_map[0xE] = {"desc": "Channel 4 Emphasis Amplitude", "val":0};
vtrx_reg_map[0x14] = {"desc": "Status", "val":0};
vtrx_reg_map[0x15] = {"desc": "ID", "val":0};
vtrx_reg_map[0x16] = {"desc": "Unique ID 1", "val":0};
vtrx_reg_map[0x17] = {"desc": "Unique ID 2", "val":0};
vtrx_reg_map[0x18] = {"desc": "Unique ID 3", "val":0};
vtrx_reg_map[0x19] = {"desc": "Unique ID 4", "val":0};
vtrx_reg_map[0x1A] = {"desc": "SEU counter 1", "val":0};
vtrx_reg_map[0x1B] = {"desc": "SEU counter 2", "val":0};
vtrx_reg_map[0x1C] = {"desc": "SEU counter 3", "val":0};
vtrx_reg_map[0x1D] = {"desc": "SEU counter 4", "val":0};

def u32_to_bytes(val):
    """Converts u32 to 4 bytes"""
    byte3 = (val >> 24) & 0xFF
    byte2 = (val >> 16) & 0xFF
    byte1 = (val >> 8) & 0xFF
    byte0 = (val >> 0) & 0xFF
    return (byte3, byte2, byte1, byte0)

def i2c_master_read(
        lpgbtInst,
        master_id,
        slave_address,
        read_len,
        reg_address_width,
        reg_address,
        timeout=0.1,
        addr_10bit=False,
    ):
    data = lpgbtInst.i2c_master_read(master_id, slave_address, read_len, reg_address_width, lpgbtInst.decode_reg_addr(reg_address), timeout=0.1, addr_10bit=False)
    if len(data) == 1:
        return data[0]
    return data

def i2c_master_write(
        lpgbtInst,
        i2cm,
        slave_address,
        address_width,
        reg_address,
        new_reg_val
    ):
    lpgbtInst.i2c_master_write(i2cm, slave_address, address_width, lpgbtInst.decode_reg_addr(reg_address), new_reg_val)

def get_tt_add_map():
    ttAddressMap = {}
    ttAddressMap[0x70] = "Master lpGBT"
    ttAddressMap[0x71] = "Master lpGBT"
    ttAddressMap[0x72] = "Slave lpGBT #1 (VFE 1-2)"
    ttAddressMap[0x73] = "Slave lpGBT #2 (VFE 3-4)"
    ttAddressMap[0x74] = "Slave lpGBT #3 (VFE 5)"
    ttAddressMap[0x50] = "VTRx"
    for i in range (1,6):
        ttAddressMap[i*16] = "ADC0" #0x10
        ttAddressMap[i*16+1] = "ADC1" #0x11
        ttAddressMap[i*16+2] = "DTU" #0x12
        ttAddressMap[i*16+3] = "CATIA" #0x13
    return ttAddressMap 

def get_vfe_parents(vfe_num):
    #slave lpGBT address, slave lpGBT master
    vfe_parent_map = [ (0x72, 1), #VFE #1
                       (0x72, 2), #VFE #2
                       (0x73, 1), #VFE #3
                       (0x73, 2), #VFE #4
                       (0x74, 1)  #VFE #5
                     ]
    return vfe_parent_map[vfe_num - 1]


def get_temps_slaves(lpgbtInst):
    slavelist = []
    for device_addr in range(100,120): #shortcut because we know where we should expect slpgbt addresses
      try:
        # To find a device we can use width = 1 even if it is not true
        # Simply we don't rely on the value we get back
        i2c_master_read(lpgbtInst, 2, device_addr, 1, 1, 0x00)
        slavelist.append(device_addr)
      except Exception as e: # exception if read_byte fails
        pass
    
    temp_list = {}
    for slave_address in slavelist:
        vref_reg = i2c_master_read(lpgbtInst, 2, slave_address, 1, 2, lpgbtInst.VREFCNTR.address)
        #temp_list[slave_address] = vref_reg

        if not(vref_reg & lpgbtInst.VREFCNTR.VREFENABLE.bit_mask):
            vref_value = (1 << 7)+vref_reg
        else:
            vref_value = vref_reg
        i2c_master_write(lpgbtInst,2, slave_address, 2, lpgbtInst.VREFCNTR.address, vref_value)
        
        inp = lpgbtInst.AdcInputSelect.TEMP
        assert inp in range(16), "Invalid ADC channel selection"
        inn = lpgbtInst.AdcInputSelect.VREF2
        assert inn in range(16), "Invalid ADC channel selection"
        gain = lpgbtInst.AdcGainSelect.X2
        assert gain in range(4), "Invalid ADC gain configuration"
    
        config = (
            lpgbtInst.ADCCONFIG.ADCENABLE.bit_mask
            | gain << lpgbtInst.ADCCONFIG.ADCGAINSELECT.offset
        )
        i2c_master_write(lpgbtInst,2, slave_address, 2, lpgbtInst.ADCCONFIG, config)
    
        mux = (
            inp << lpgbtInst.ADCSELECT.ADCINPSELECT.offset
            | inn << lpgbtInst.ADCSELECT.ADCINNSELECT.offset
        )
        i2c_master_write(lpgbtInst,2, slave_address, 2, lpgbtInst.ADCSELECT, mux)
    
    #    sensor_temp = round((lpgbtInst.adc_convert(1) - 486.2)/2.105, 1)
        results = []
        config = i2c_master_read(lpgbtInst, 2, slave_address, 1, 2, lpgbtInst.ADCCONFIG)
        
        samples = 1
        for _ in range(samples):
            i2c_master_write(lpgbtInst,2, slave_address, 2, lpgbtInst.ADCCONFIG, config | lpgbtInst.ADCCONFIG.ADCCONVERT.bit_mask)
            timeout = 0.001
            timeout_time = time.time() + timeout
            while True:
                statush, statusl = i2c_master_read(lpgbtInst, 2, slave_address, 2, 2, lpgbtInst.ADCSTATUSH)
    
                if statush & lpgbtInst.ADCSTATUSH.ADCDONE.bit_mask:
                    results.append(statusl | (statush & 0x3) << 8)
                    break
    
                if time.time() > timeout_time:
                    # stop the measurement
                    i2c_master_write(lpgbtInst,2, slave_address, 2, lpgbtInst.ADCCONFIG, config)
                    raise LpgbtTimeoutError("Timeout while waiting for ADC to finish")
            i2c_master_write(lpgbtInst,2, slave_address, 2, lpgbtInst.ADCCONFIG, config)
    
        if len(results) == 1:
            results = results[0]
    
        sensor_temp = round((results - 486.2)/2.105, 1)
        temp_list[slave_address] = sensor_temp 
    return temp_list

 
def get_fe_device_addresses(lpgbtInst, master_addr=0):
    # We cannot use a generic scan because only lpgbts can have slave devices to contact
    # But lpGBTs have 3 different master i2c, so the hierarchy is not simple enough to
    # perform a normal loop scan.
    if (master_addr ==0): master_addr = 113
    devices = {}
    # Scan first level
    # We expect to find the master lpgbt, which we know it has 3 master i2cs
    slave_list = [0]
    slave_list.extend(range(110,120))

    #mlpgbts = lpgbtInst.scan_smbus()
    #for mlpgbt in mlpgbts:
    #mlpgbt = lpgbtInst._current_comm_intf.get_i2c_address()
#    print(lpgbtInst.getLpgbtLowLevelInst().get_i2c_address())
#    devices[lpgbtInst.getLpgbtLowLevelInst().get_i2c_address()]={0:{}, 1:{}, 2:{}}
    devices[master_addr]={0:{}, 1:{}, 2:{}}

    # Scan second level
    for master_lpgbt in devices:
        for master_id in devices[master_lpgbt]:
            min_range = 1
            max_range = 128
            if master_id == 0: # this version of lpgbt has a problem with master i2c #0 so we just skip it
                continue
            if master_id == 1: # shortcut because we already know the addresses
                min_range = 75
                max_range = 85
            if master_id == 2: # shortcut because we already know the addresses
                min_range = 100
                max_range = 120
            for device_addr in range(min_range,max_range):
                try:
                    # To find a device we can use width = 1 even if it is not true
                    # Simply we don't rely on the value we get back
                    i2c_master_read(lpgbtInst, master_id, device_addr, 1, 1, 0x00)

                    #self.i2c_master_read(master_id, device_addr, 1, 0x000)
                    # On i2c master 2, we expect to find the slave lpgbts with 3 master i2c each
                    if master_id == 2:
                        devices[master_lpgbt][master_id][device_addr] = {0:[], 1:[], 2:[]}
                    else:
                        devices[master_lpgbt][master_id][device_addr] = {}
                except Exception as e: # exception if read_byte fails
                    pass
    return devices

def get_slave_device_regs(lpgbtInst, slave_device_add):
    print("In get_slave_device_regs with address", slave_device_add)
    result = []
    address_map = get_tt_add_map()
    if 'Slave' in address_map[int(slave_device_add)]:
        print("Request of slave lpGBT registers. Collecting values...")
        
        for address in range(len(lpgbtInst.Reg)):
            if address % 15 == 0:
                nbytes = 15 if len(lpgbtInst.Reg) - address >= 15 else len(lpgbtInst.Reg) - address
                data = i2c_master_read(lpgbtInst, 2, int(slave_device_add), nbytes, 2, address)
                for cnt, onebyte in enumerate(data):
                    data_bin = '{0:08b}'.format(onebyte)
                    name = lpgbtInst.Reg(address+cnt).name,
                    reg_name = "%-28s" % name
                    result.append({"Address": hex(address+cnt), "Name": reg_name, "Value": "0x%02X" % onebyte})
            #nbytes = 1
            #data = i2c_master_read(lpgbtInst, 2, int(slave_device_add), nbytes, 2, address)
            #name = lpgbtInst.Reg(address).name,
            #reg_name = "%-28s" % name
            #result.append({"Address": hex(address), "Name": reg_name, "Value": "0x%02X" % data})
            #print(hex(address), hex(data))
            
    return result

def get_slpgbt_state(lpgbtInst, slave_address):
    """Returns the current Power-Up State Machine state"""
#    return self.read_reg(self.PUSMSTATUS)    
    return i2c_master_read(lpgbtInst, 2, int(slave_address), 1, 2, lpgbtInst.PUSMSTATUS)

def detect_slpgbt_chip_version(lpgbtInst, slpgbt_address):
    lpgbt_address = 0

    chip_version = 999
    try:
        #if self.lpgbt_read(lpgbt_address, 0x1c5) == 0xa5:
        if i2c_master_read(lpgbtInst, 2, slpgbt_address, 1, 2, 0x1c5) == 0xa5:
            chip_version = 0
        #elif self.lpgbt_read(lpgbt_address, 0x1d7) == 0xa6:
        elif i2c_master_read(lpgbtInst, 2, slpgbt_address, 1, 2, 0x1d7) == 0xa6:
            chip_version = 1
    except:
        pass

    return chip_version

def get_slpgbt_chipid_fuses_(lpgbtInst, slpgbt_address):
    """ From function _get_chipid(self.fuses_read_bank) in 
        /home/pi/.local/lib/python3.7/site-packages/lpgbt_control_lib/lpgbt.py
    """
    print("in get_slpgbt_chipid_fuses_", slpgbt_address)
    chipid = fuses_read_bank(lpgbtInst, slpgbt_address, 0)
 
    return chipid

def get_slpgbt_process_monitor(lpgbtInst, slpgbt_address, channel, timeout=1):
        """Enables process monitor channel and reads frequency

        Arguments:
            channel: process monitor channel to use
            timeout: measurement completion timeout
        """
        assert channel in range(4), "Invalid process monitor channel"

        #reg_val_masked = self.read_reg(self.PROCESSANDSEUMONITOR) & (
        reg_val_masked = i2c_master_read(lpgbtInst, 2, slpgbt_address, 1, 2, lpgbtInst.PROCESSANDSEUMONITOR) & (
            ~(
                lpgbtInst.PROCESSANDSEUMONITOR.PMCHANNEL.bit_mask
                + lpgbtInst.PROCESSANDSEUMONITOR.PMENABLE.bit_mask
            )  # pylint: disable=invalid-unary-operand-type
        )
        cntr_reg = reg_val_masked | (
            channel << lpgbtInst.PROCESSANDSEUMONITOR.PMCHANNEL.offset
        )
        #self.write_reg(self.PROCESSANDSEUMONITOR, cntr_reg)
        i2c_master_write(lpgbtInst,2, slpgbt_address, 2, lpgbtInst.decode_reg_addr(lpgbtInst.PROCESSANDSEUMONITOR), cntr_reg)
        cntr_reg |= lpgbtInst.PROCESSANDSEUMONITOR.PMENABLE.bit_mask
        #self.write_reg(self.PROCESSANDSEUMONITOR, cntr_reg)
        i2c_master_write(lpgbtInst,2, slpgbt_address, 2, lpgbtInst.decode_reg_addr(pgbtInst.PROCESSANDSEUMONITOR), cntr_reg)
        timeout_time = time.time() + timeout
        while True:
            #status = self.read_reg(self.PROCESSMONITORSTATUS)
            status = i2c_master_read(lpgbtInst, 2, slpgbt_address, 1, 2, lpgbtInst.PROCESSMONITORSTATUS)
            if status & lpgbtInst.PROCESSMONITORSTATUS.PMDONE.bit_mask:
                break
            if time.time() > timeout_time:
                raise LpgbtTimeoutError(
                    f"Timeout while waiting for process monitor (status: 0x{status:02x})"
                )
        #results = self.read_regs(self.PMFREQA, read_len=3)
        results = i2c_master_read(lpgbtInst, 2, slpgbt_address, 3, 2, lpgbtInst.PMFREQA)
        #self.write_reg(self.PROCESSANDSEUMONITOR, reg_val_masked)
        i2c_master_write(lpgbtInst,2, slpgbt_address, 2, lpgbtInst.decode_reg_addr(lpgbtInst.PROCESSANDSEUMONITOR), reg_val_masked)

        return results[0] << 16 | results[1] << 8 | results[2]

def read_slpgbt_mode(lpgbtInst, slpgbt_address):
    """Returns the lpGBT mode from the CONFIGPINS register"""
#    configpins = self.read_reg(self.CONFIGPINS)
    configpins = i2c_master_read(lpgbtInst, 2, slpgbt_address, 1, 2, lpgbtInst.CONFIGPINS)
    return configpins >> lpgbtInst.CONFIGPINS.LPGBTMODE.offset

def config_watchdog(
        lpgbtInst, slpgbt_address,
        pll_watchdog_enable=True,
        dll_watchdog_enable=True,
        checksum_watchdog_enable=False,
    ):
        """Configure watchdog

        Arguments:
            pll_watchdog_enable: enables PLL watchdog
            dll_watchdog_enable: enables DLL watchdog
            checksum_watchdog_enable: enables checksum watchdog
        """

        reg_val = 0x0
        if not pll_watchdog_enable:
            reg_val |= lpgbtInst.WATCHDOG.PUSMPLLWDOGDISABLE.bit_mask
        if not dll_watchdog_enable:
            reg_val |= lpgbtInst.WATCHDOG.PUSMDLLWDOGDISABLE.bit_mask
        if checksum_watchdog_enable:
            reg_val |= lpgbtInst.WATCHDOG.PUSMCHECKSUMWDOGENABLE.bit_mask
#        self.write_reg(self.WATCHDOG, reg_val)
        i2c_master_write(lpgbtInst,2, slpgbt_address, 2, lpgbtInst.decode_reg_addr(lpgbtInst.WATCHDOG), reg_val)


def slpgbt_bert_run(lpgbtInst, slpgbt_address):
        """Executes the lpGBT BERT

        Arguments:
            ignore_no_activity: disregard BERT error flag



        Raises:
            LpgbtException: if timeout or not data at the input

        Returns:
            Dictionary containing BER test information.
        """
#        config = self.read_reg(self.BERTCONFIG)
        config = i2c_master_read(lpgbtInst, 2, slpgbt_address, 1, 2, lpgbtInst.BERTCONFIG)
        
        meas_time = (config >> lpgbtInst.BERTCONFIG.BERTMEASTIME.offset) & 0x0F
        bits = 2 ** (5 + 2 * meas_time)

#        lpgbtInst.write_reg(lpgbtInst.BERTCONFIG, config | lpgbtInst.BERTCONFIG.BERTSTART.bit_mask)
        i2c_master_write(lpgbtInst,2, slpgbt_address, 2, lpgbtInst.BERTCONFIG, config | lpgbtInst.BERTCONFIG.BERTSTART.bit_mask)

        timeout = 1  # should be computed based on meas_time
        timeout_time = time.time() + timeout
        status = 0
        no_data = False
        while True:
#            status = lpgbtInst.read_reg(lpgbtInst.BERTSTATUS)
            status = i2c_master_read(lpgbtInst, 2, slpgbt_address, 1, 2, lpgbtInst.BERTSTATUS)
            if status & lpgbtInst.BERTSTATUS.BERTDONE.bit_mask:
                break

            if (
                not ignore_no_activity
                and status & lpgbtInst.BERTSTATUS.BERTPRBSERRORFLAG.bit_mask
            ):
                lpgbtInst.logger.warning(
                    "BERT error flag (there was not data on the input?)"
                )
                no_data = True
                break

            if time.time() > timeout_time:
#                lpgbtInst.write_reg(lpgbtInst.BERTCONFIG, config)  # stop measurement and fail
                i2c_master_write(lpgbtInst,2, slpgbt_address, 2, lpgbtInst.BERTCONFIG, config)  # stop measurement and fail
                raise LpgbtTimeoutError("Timeout while waiting for BERT to finish")

        if no_data:
            errors = bits / 2
        else:
            errors = (
#                lpgbtInst.read_reg(lpgbtInst.BERTRESULT0)
#                | lpgbtInst.read_reg(lpgbtInst.BERTRESULT1) << 8
#                | lpgbtInst.read_reg(lpgbtInst.BERTRESULT2) << 16
#                | lpgbtInst.read_reg(lpgbtInst.BERTRESULT3) << 24
#                | lpgbtInst.read_reg(lpgbtInst.BERTRESULT4) << 32
                i2c_master_read(lpgbtInst, 2, slpgbt_address, 1, 2, lpgbtInst.BERTRESULT0)
                | i2c_master_read(lpgbtInst, 2, slpgbt_address, 1, 2, lpgbtInst.BERTRESULT1) << 8
                | i2c_master_read(lpgbtInst, 2, slpgbt_address, 1, 2, lpgbtInst.BERTRESULT2) << 16
                | i2c_master_read(lpgbtInst, 2, slpgbt_address, 1, 2, lpgbtInst.BERTRESULT3) << 24
                | i2c_master_read(lpgbtInst, 2, slpgbt_address, 1, 2, lpgbtInst.BERTRESULT4) << 32
            )
            errors /= 3
#        lpgbtInst.write_reg(lpgbtInst.BERTCONFIG, config)
        i2c_master_write(lpgbtInst, 2, slpgbt_address, 2, lpgbtInst.BERTCONFIG, config)
        result = {
            "bits": bits,
            "errors": errors,
            "error_flag": no_data,
            "ber": errors / bits,
        }
        return result

def slpgbt_line_driver_set_data_source(lpgbtInst, slpgbt_address, source):

    assert source in range(4), "Invalid line driver data source"

    i2c_master_write(lpgbtInst, 2, slpgbt_address, 2, lpgbtInst.ULDATASOURCE1, source << lpgbtInst.ULDATASOURCE1.LDDATASOURCE.offset)

def slpgbt_serializer_set_data_source(lpgbtInst, slpgbt_address, source):

    assert source in range(16), "Invalid serializer data source"

    i2c_master_write(lpgbtInst, 2, slpgbt_address, 2, lpgbtInst.ULDATASOURCE0, source)


def slpgbt_uplink_set_constant_pattern(lpgbtInst, slpgbt_address, constant_pattern):
        """Sets the constant uplink pattern. Constant pattern transmission
        must still be manually enabled by use of line_driver_set_data_source
        or serializer_set_data_source.

        Arguments:
            constant_pattern: 32 bit constant pattern value
        """
        frame = u32_to_bytes(constant_pattern)
        i2c_master_write(lpgbtInst, 2, slpgbt_address, 2, lpgbtInst.DPDATAPATTERN3, frame[3])  # pattern for group 3
        i2c_master_write(lpgbtInst, 2, slpgbt_address, 2, lpgbtInst.DPDATAPATTERN2, frame[2])  # pattern for group 2
        i2c_master_write(lpgbtInst, 2, slpgbt_address, 2, lpgbtInst.DPDATAPATTERN1, frame[1])  # pattern for group 1
        i2c_master_write(lpgbtInst, 2, slpgbt_address, 2, lpgbtInst.DPDATAPATTERN0, frame[0])  # pattern for group 0

def slpgbt_bert_set_constant_pattern(lpgbtInst, slpgbt_address, constant_pattern):
        """Configures the BERT constant pattern checker.

        Arguments:
            constant_pattern: 32 bit constant pattern to check
        """
        frame = u32_to_bytes(constant_pattern)
        i2c_master_write(lpgbtInst,2, slpgbt_address, 2, lpgbtInst.BERTDATAPATTERN3, frame[3])  # pattern for group 3
        i2c_master_write(lpgbtInst,2, slpgbt_address, 2, lpgbtInst.BERTDATAPATTERN2, frame[2])  # pattern for group 2
        i2c_master_write(lpgbtInst,2, slpgbt_address, 2, lpgbtInst.BERTDATAPATTERN1, frame[1])  # pattern for group 1
        i2c_master_write(lpgbtInst,2, slpgbt_address, 2, lpgbtInst.BERTDATAPATTERN0, frame[0])  # pattern for group 0

def slpgbt_downlink_set_data_source(lpgbtInst, slpgbt_address, group_id, source):
        """Configures the downlink data source

        Arguments:
            group_id: downlink data path group number
            source: data path data source for group_id
        """
        assert group_id in range(4), "Invalid downlink data path group"
        assert source in range(4), "Invalid downlink data source"

        reg_addr = lpgbtInst.ULDATASOURCE5
        bit_pos = (group_id % 4) * 2
#        reg_val = lpgbtInst.read_reg(reg_addr)

        reg_val = i2c_master_read(lpgbtInst, 2, slpgbt_address, 1, 2, reg_addr)
        reg_val &= ~(0x3 << bit_pos)
        reg_val |= source << bit_pos
#        self.write_reg(reg_addr, reg_val)
        i2c_master_write(lpgbtInst,2, slpgbt_address, 2, reg_addr, reg_val)

def slpgbt_uplink_set_data_source(lpgbtInst, slpgbt_address, group_id, source):
        """Configures the uplink data source

        Arguments:
            group_id: uplink data path group number
            source: data path data source for group_id
        """
        assert group_id in range(7), "Invalid uplink data path group"
        assert source in range(8), "Invalid uplink data source"
        grp_mapping = (
            (lpgbtInst.ULDATASOURCE1, lpgbtInst.ULDATASOURCE1.ULG0DATASOURCE.offset),
            (lpgbtInst.ULDATASOURCE1, lpgbtInst.ULDATASOURCE1.ULG1DATASOURCE.offset),
            (lpgbtInst.ULDATASOURCE2, lpgbtInst.ULDATASOURCE2.ULG2DATASOURCE.offset),
            (lpgbtInst.ULDATASOURCE2, lpgbtInst.ULDATASOURCE2.ULG3DATASOURCE.offset),
            (lpgbtInst.ULDATASOURCE3, lpgbtInst.ULDATASOURCE3.ULG4DATASOURCE.offset),
            (lpgbtInst.ULDATASOURCE3, lpgbtInst.ULDATASOURCE3.ULG5DATASOURCE.offset),
            (lpgbtInst.ULDATASOURCE4, lpgbtInst.ULDATASOURCE4.ULG6DATASOURCE.offset),
        )
        reg_addr, bit_pos = grp_mapping[group_id]
#        reg_val = self.read_reg(reg_addr)
        reg_val = i2c_master_read(lpgbtInst, 2, slpgbt_address, 1, 2, reg_addr)
        reg_val &= ~(0x7 << bit_pos)
        reg_val |= source << bit_pos
#        self.write_reg(reg_addr, reg_val)
        i2c_master_write(lpgbtInst,2, slpgbt_address, 2, reg_addr, reg_val)

def slpgbt_eprx_prbs_gen_enable(lpgbtInst, slpgbt_address, group_id, channel_id, enable=True):
        """Enables PRBS generator attached to a ePortRx group/channel input.
        The PRBS feature requires that phase shifter clock 0 is enabled
        and set to an appropriate frequency.

        Arguments:
            group_id: ePortRx group to configure
            channel_id: ePortRx channel to configure
            enable: PRBS generator state
        """
        assert group_id in range(7), "Invalid ePortRx group"
        assert channel_id in range(4), "Invalid ePortRx channel"

        offset = channel_id + group_id * 4
        reg = lpgbtInst.EPRXPRBS0.address - int(offset / 8)
        bit = offset % 8

#        value = self.read_reg(reg)
        value = i2c_master_read(lpgbtInst, 2, slpgbt_address, 1, 2, reg)
        if enable:
            value |= 1 << bit
        else:
            value &= ~(1 << bit)
#        self.write_reg(reg, value)
        i2c_master_write(lpgbtInst,2, slpgbt_address, 2, reg, value)

def slpgbt_bert_config(lpgbtInst, slpgbt_address, meas_time, source, skip_disable=False):
        """Configures the lpGBT BERT

        Arguments:
            meas_time: measurement cycle
            source: data source
            skip_disable: disable checker skipping
        """
        assert meas_time in range(16), "Invalid BERT measurement time"
        assert source in range(256), "Invalid BERT source configuration"

#        self.write_reg(self.BERTSOURCE, source)
        i2c_master_write(lpgbtInst,2, slpgbt_address, 2, lpgbtInst.BERTSOURCE, source)
        config = meas_time << lpgbtInst.BERTCONFIG.BERTMEASTIME.offset
        if skip_disable:
            config |= lpgbtInst.BERTCONFIG.SKIPDISABLE.bit_mask
#        self.write_reg(self.BERTCONFIG, config)
        i2c_master_write(lpgbtInst,2, slpgbt_address, 2, lpgbtInst.BERTCONFIG, config)

def slpgbt_clock_generator_setup(
    lpgbtInst, slpgbt_address
):

        i2c_master_write(lpgbtInst, 2, slpgbt_address, 2,
            lpgbtInst.REFCLK,
            lpgbtInst.REFCLK.REFCLKACBIAS.bit_mask | lpgbtInst.REFCLK.REFCLKTERM.bit_mask
        )

        i2c_master_write(lpgbtInst, 2, slpgbt_address, 2,
            lpgbtInst.CLKGCONFIG0,
            (
                0x0E << lpgbtInst.CLKGCONFIG0.CLKGCALIBRATIONENDOFCOUNT.offset
                | 0x08 << lpgbtInst.CLKGCONFIG0.CLKGBIASGENCONFIG.offset
            )
        )
        i2c_master_write(lpgbtInst, 2, slpgbt_address, 2,
            lpgbtInst.CLKGCONFIG1,
            (
                lpgbtInst.CLKGCONFIG1.CLKGCDRRES.bit_mask
                | lpgbtInst.CLKGCONFIG1.CLKGVCORAILMODE.bit_mask
                | 8 << lpgbtInst.CLKGCONFIG1.CLKGVCODAC.offset
            )
        )
        i2c_master_write(lpgbtInst, 2, slpgbt_address, 2,
            lpgbtInst.CLKGPLLINTCUR,
            (
                9 << lpgbtInst.CLKGPLLINTCUR.CLKGPLLINTCURWHENLOCKED.offset
                | 9 << lpgbtInst.CLKGPLLINTCUR.CLKGPLLINTCUR.offset
            )
        )
        i2c_master_write(lpgbtInst, 2, slpgbt_address, 2,
            lpgbtInst.CLKGPLLPROPCUR,
            (
                9 << lpgbtInst.CLKGPLLPROPCUR.CLKGPLLPROPCURWHENLOCKED.offset
                | 9 << lpgbtInst.CLKGPLLPROPCUR.CLKGPLLPROPCUR.offset
            )
        )
        i2c_master_write(lpgbtInst, 2, slpgbt_address, 2,
            lpgbtInst.CLKGPLLRES,
            (
                2 << lpgbtInst.CLKGPLLRES.CLKGPLLRESWHENLOCKED.offset
                | 2 << lpgbtInst.CLKGPLLRES.CLKGPLLRES.offset
            )
        )
        i2c_master_write(lpgbtInst, 2, slpgbt_address, 2,
            lpgbtInst.CLKGFFCAP,
            (
                3 << lpgbtInst.CLKGFFCAP.CLKGFEEDFORWARDCAPWHENLOCKED.offset
                | 3 << lpgbtInst.CLKGFFCAP.CLKGFEEDFORWARDCAP.offset
            )
        )
        i2c_master_write(lpgbtInst, 2, slpgbt_address, 2,
            lpgbtInst.CLKGCDRINTCUR,
            (
                5 << lpgbtInst.CLKGCDRINTCUR.CLKGCDRINTCURWHENLOCKED.offset
                | 5 << lpgbtInst.CLKGCDRINTCUR.CLKGCDRINTCUR.offset
            )
        )
        i2c_master_write(lpgbtInst, 2, slpgbt_address, 2,
            lpgbtInst.CLKGFLLINTCUR,
            (
                5 << lpgbtInst.CLKGFLLINTCUR.CLKGFLLINTCURWHENLOCKED.offset
                | 5 << lpgbtInst.CLKGFLLINTCUR.CLKGFLLINTCUR.offset
            )
        )
        i2c_master_write(lpgbtInst, 2, slpgbt_address, 2,
            lpgbtInst.CLKGCDRPROPCUR,
            (
                5 << lpgbtInst.CLKGCDRPROPCUR.CLKGCDRPROPCURWHENLOCKED.offset
                | 5 << lpgbtInst.CLKGCDRPROPCUR.CLKGCDRPROPCUR.offset
            )
        )
        i2c_master_write(lpgbtInst, 2, slpgbt_address, 2,
            lpgbtInst.CLKGCDRFFPROPCUR,
            (
                6 << lpgbtInst.CLKGCDRFFPROPCUR.CLKGCDRFEEDFORWARDPROPCURWHENLOCKED.offset
                | 6 << lpgbtInst.CLKGCDRFFPROPCUR.CLKGCDRFEEDFORWARDPROPCUR.offset
            )
        )
        i2c_master_write(lpgbtInst, 2, slpgbt_address, 2,
            lpgbtInst.CLKGLFCONFIG0,
            (
                lpgbtInst.CLKGLFCONFIG0.CLKGLOCKFILTERENABLE.bit_mask
                | 15 << lpgbtInst.CLKGLFCONFIG0.CLKGLOCKFILTERLOCKTHRCOUNTER.offset
            )
        )
        i2c_master_write(lpgbtInst, 2, slpgbt_address, 2,
            lpgbtInst.CLKGLFCONFIG1,
            (
                15 << lpgbtInst.CLKGLFCONFIG1.CLKGLOCKFILTERRELOCKTHRCOUNTER.offset
                | 15 << lpgbtInst.CLKGLFCONFIG1.CLKGLOCKFILTERUNLOCKTHRCOUNTER.offset
            )
        )
        i2c_master_write(lpgbtInst, 2, slpgbt_address, 2,
            lpgbtInst.CLKGWAITTIME,
            (8 << lpgbtInst.CLKGWAITTIME.CLKGWAITCDRTIME.offset)
            | 8 << lpgbtInst.CLKGWAITTIME.CLKGWAITPLLTIME.offset
        )

        lpgbtInst.eprx_general_config()


def slpgbt_line_driver_setup(
    lpgbtInst, slpgbt_address,
    modulation_current=64,
    emphasis_enable=False,
    emphasis_short=False,
    emphasis_amp=32,
):

    assert modulation_current in range(128), "Invalid modulation current value"
    assert emphasis_amp in range(128), "Invalid modulation pre-emphasis current value"

    modulation_current = int(modulation_current) & 0x7F
    ldconfig_h = modulation_current << lpgbtInst.LDCONFIGH.LDMODULATIONCURRENT.offset
    if emphasis_enable:
        ldconfig_h |= lpgbtInst.LDCONFIGH.LDEMPHASISENABLE.bit_mask

    ldconfig_l = emphasis_amp << lpgbtInst.LDCONFIGL.LDEMPHASISAMP.offset
    if emphasis_short:
        ldconfig_l |= lpgbtInst.LDCONFIGL.LDEMPHASISSHORT.bit_mask

    #lpgbtInst.write_reg(lpgbtInst.LDCONFIGH, ldconfig_h)
    #lpgbtInst.write_reg(lpgbtInst.LDCONFIGL, ldconfig_l)

    i2c_master_write(lpgbtInst, 2, slpgbt_address, 2, lpgbtInst.LDCONFIGH, ldconfig_h)
    i2c_master_write(lpgbtInst, 2, slpgbt_address, 2, lpgbtInst.LDCONFIGL, ldconfig_l)

def slpgbt_equalizer_setup(lpgbtInst, slpgbt_address, attenuation=3, cap=0, res0=0, res1=0, res2=0, res3=0):
    # pylint: disable=too-many-arguments
    assert attenuation in range(4), "Invalid attentuation"
    assert cap in range(4), "Invalid capacitor setting"
    assert res0 in range(4), "Invalid resistor 0 setting"
    assert res1 in range(4), "Invalid resistor 1 setting"
    assert res2 in range(4), "Invalid resistor 2 setting"
    assert res3 in range(4), "Invalid resistor 3 setting"

    eq_config = (
        attenuation << lpgbtInst.EQCONFIG.EQATTENUATION.offset
        | cap << lpgbtInst.EQCONFIG.EQCAP.offset
    )
#    lpgbtInst.write_reg(lpgbtInst.EQCONFIG, eq_config)
    i2c_master_write(lpgbtInst, 2, slpgbt_address, 2, lpgbtInst.EQCONFIG, eq_config)
    eq_res = (
        res3 << lpgbtInst.EQRES.EQRES3.offset
        | res2 << lpgbtInst.EQRES.EQRES2.offset
        | res1 << lpgbtInst.EQRES.EQRES1.offset
        | res0 << lpgbtInst.EQRES.EQRES0.offset
    )
#    lpgbtInst.write_reg(lpgbtInst.EQRES, eq_res)
    i2c_master_write(lpgbtInst, 2, slpgbt_address, 2, lpgbtInst.EQRES, eq_res)


def slpgbt_config_done(lpgbtInst, slpgbt_address, pll_config_done=True, dll_config_done=True):
    reg_val = 0
    if pll_config_done:
        reg_val |= lpgbtInst.POWERUP2.PLLCONFIGDONE.bit_mask
    if dll_config_done:
        reg_val |= lpgbtInst.POWERUP2.DLLCONFIGDONE.bit_mask

#        self.write_reg(self.POWERUP2, reg_val)
    i2c_master_write(lpgbtInst, 2, slpgbt_address, 2, lpgbtInst.POWERUP2, reg_val)

def reset_slave_lpgbts(lpgbtInst):
    print('Resetting the slave LpGBTs setting to low the RSTB pin')
    print('To do that we need to set to low the GPIO0 of the master lpGBT')

    print("Reading if GPIOs are input or output:")
    gpiodirl = lpgbtInst.read_reg(lpgbtInst.PIODIRL)
    print("Reading output values of GPIOs:")
    gpiooutl = lpgbtInst.read_reg(lpgbtInst.PIOOUTL)
    print("Setting GPIO0 as output and with value low for 0.01 sec")
    gpiodirl_new = gpiodirl | 0x1
    gpiodirl_new_high = gpiodirl | 0x1
    gpiodirl_new_low = gpiodirl & 0xFE
    lpgbtInst.write_reg(lpgbtInst.PIODIRL, gpiodirl_new)
    lpgbtInst.write_reg(lpgbtInst.PIOOUTL, gpiodirl_new_high)
    time.sleep(1)
    lpgbtInst.write_reg(lpgbtInst.PIOOUTL, gpiodirl_new_low)
    time.sleep(0.01)
    lpgbtInst.write_reg(lpgbtInst.PIOOUTL, gpiodirl_new_high)
    lpgbtInst.write_reg(lpgbtInst.PIODIRL, gpiodirl)
    print("... and back to original value")

def load_our_config_file(lpgbtInst):

    file_path = '/home/pi/pigbt-ecal-v1/regs/master_rom_ec_v33.cnf'
    print("Write configuration from file {}".format(file_path))
    with open(file_path, 'r') as f:
        for line in f:
            l = line.strip()
            if l.startswith("#"):
               continue
            ll = l.split()
            if len(ll) >= 2:
                addr = int(ll[0], 0)
                val = int(ll[1], 0)
                lpgbtInst.write_reg(addr, val)

def map_slpgbt_files(slpgbt):
    file_list = {'114':'/home/pi/fe_lpgbt_config_files/slave_1_v33.cnf', '115':'/home/pi/fe_lpgbt_config_files/slave_v33.cnf', '116':'/home/pi/fe_lpgbt_config_files/slave_v33.cnf'}
    return file_list[slpgbt]

def load_our_config_file_in_slpgbt(lpgbtInst, slpgbt_device_add):
    file_path = map_slpgbt_files(slpgbt_device_add)
    print("Write configuration from file {}".format(file_path))
    with open(file_path, 'r') as f:
        for line in f:
            l = line.strip()
            if l.startswith("#"):
               continue
            ll = l.split()
            if len(ll) >= 2:
                addr = int(ll[0], 0)
                val = int(ll[1], 0)
                i2c_master_write(lpgbtInst, 2, int(slpgbt_device_add), 2, int(addr), int(val))

def check_slpgbt_config(lpgbtInst, slpgbt_device_add):
    file_path = map_slpgbt_files(slpgbt_device_add)
    print("Checking against configuration from file {}".format(file_path))
    check_pass = True
    with open(file_path, 'r') as f:
        for line in f:
            l = line.strip()
            if l.startswith("#"):
                continue
            ll = l.split()
            if len(ll) >= 2:
                addr = int(ll[0], 0)
                val = int(ll[1], 0)
                data = i2c_master_read(lpgbtInst, 2, int(slpgbt_device_add), 1, 2, int(addr))
                if (val != data):
                    check_pass = False
                    print('Register {} target value {} but read{}'.format(addr,val,data))
    return check_pass

def get_vfe_device_addresses(lpgbtInst, lpgbt_slave_add, lpgbt_slave_master_num):

    vfe_addresses = {}
    counter_ADC0 = 1
    counter_ADC1 = 1
    counter_DTU = 1
    counter_CATIA = 1

    for slave_slave_device_1 in range(1,6):
        for slave_slave_device_2 in range(0,4):

            slave_slave_device = slave_slave_device_1*16 + slave_slave_device_2

            try:
                i2c_slave_read(
                    lpgbtInst=lpgbtInst,
                    master_id=2,
                    slave_address=slave_slave_device,
                    read_len=1,
                    reg_address_width=1,
                    reg_address=0x00,
                    slpgbt_device_add=lpgbt_slave_add,
                    slpgbt_master_id=lpgbt_slave_master_num,
                )
                if(slave_slave_device_2 == 0):
                    vfe_addresses["ADC0_"+str(counter_ADC0)]=slave_slave_device
                    counter_ADC0 += 1
                if(slave_slave_device_2 == 1):
                    vfe_addresses["ADC1_"+str(counter_ADC1)]=slave_slave_device
                    counter_ADC1 += 1
                if(slave_slave_device_2 ==2):
                    vfe_addresses["DTU_"+str(counter_DTU)]=slave_slave_device
                    counter_DTU += 1
                if(slave_slave_device_2 == 3):
                    #print("CATIA FOUND ", slave_slave_device)
                    vfe_addresses["CATIA_"+str(counter_CATIA)]=slave_slave_device
                    counter_CATIA += 1
            except Exception as inst:
                print(inst)
                pass
    return vfe_addresses

def get_vfe_device_regs(lpgbtInst, slpgbt_device_add, slpgbt_master_id, vfe_device_add):
    print("In get_vfe_device_regs with address", slpgbt_device_add)
    result = []
    address_map = get_tt_add_map()
    if 'DTU' in address_map[int(vfe_device_add)]:
        print("Request of DTU registers. Collecting values...")
        data_list_1 = i2c_slave_read(
                lpgbtInst=lpgbtInst,
                master_id=2,
                slave_address=vfe_device_add,
                read_len=9,
                reg_address_width=1,
                reg_address=0x00,
                slpgbt_device_add=slpgbt_device_add,
                slpgbt_master_id=slpgbt_master_id,
            )
        data_list_2 = i2c_slave_read(
                lpgbtInst=lpgbtInst,
                master_id=2,
                slave_address=vfe_device_add,
                read_len=11,
                reg_address_width=1,
                reg_address=0x09,
                slpgbt_device_add=slpgbt_device_add,
                slpgbt_master_id=slpgbt_master_id,
            )
        
        for address, data in enumerate(data_list_1 + data_list_2):
            data_bin = '{0:0b}'.format(data)
            name = REG2STR_DTU[address]
            if len(name)>20:
                name = name[:13]+"..."
            result.append({"Address":hex(address), "Name":name, "Value": "0x%02X"%data})
    elif 'CATIA' in address_map[int(vfe_device_add)]:
        print("Request of CATIA registers. Collecting values...")
        data_list_1 = i2c_slave_read(
                lpgbtInst=lpgbtInst,
                master_id=2,
                slave_address=vfe_device_add,
                read_len=5,
                reg_address_width=1,
                reg_address=0x00,
                slpgbt_device_add=slpgbt_device_add,
                slpgbt_master_id=slpgbt_master_id,
            )
        for address, data in enumerate(data_list_1):
            data_bin = '{0:0b}'.format(data)
            name = REG2STR_CATIA[address]
            if len(name)>20:
                name = name[:13]+"..."
            result.append({"Address":hex(address), "Name":name, "Value": "0x%02X"%data})

    return result

def get_all_dtu_regs(lpgbtInst, vfe_device_num):
    lpgbt_slave_address, lpgbt_slave_master_num = get_vfe_parents(vfe_device_num)
    vfe_addresses = get_vfe_device_addresses(lpgbtInst, lpgbt_slave_address, lpgbt_slave_master_num)
    print(vfe_addresses)


    reg_list_by_address = {}
    for ch in range(1,6):
        device_name = "DTU_"+str(ch)
        result = get_vfe_device_regs(lpgbtInst, lpgbt_slave_address, lpgbt_slave_master_num, vfe_addresses[device_name])
        for reg in result:
            if reg["Address"] not in reg_list_by_address:
                reg_list_by_address[reg["Address"]]={"regAddress": reg["Address"], "regName": reg["Name"]}
            reg_list_by_address[reg["Address"]][device_name] = reg["Value"]
    for key, value in reg_list_by_address.items():
        print("reg {}, value {}".format(key, value))
    return list(reg_list_by_address.values())

def get_all_catia_regs(lpgbtInst, vfe_device_num):
    lpgbt_slave_address, lpgbt_slave_master_num = get_vfe_parents(vfe_device_num)
    vfe_addresses = get_vfe_device_addresses(lpgbtInst, lpgbt_slave_address, lpgbt_slave_master_num)
    print(vfe_addresses)


    reg_list_by_address = {}
    for ch in range(1,6):
        device_name = "CATIA_"+str(ch)
        result = get_vfe_device_regs(lpgbtInst, lpgbt_slave_address, lpgbt_slave_master_num, vfe_addresses[device_name])
        for reg in result:
            if reg["Address"] not in reg_list_by_address:
                reg_list_by_address[reg["Address"]]={"regAddress": reg["Address"], "regName": reg["Name"]}
            reg_list_by_address[reg["Address"]][device_name] = reg["Value"]
    for key, value in reg_list_by_address.items():
        print("reg {}, value {}".format(key, value))
    return list(reg_list_by_address.values())

#############################################################################################
#                Functions to perform a i2c slave read and i2c slave write                  #
#############################################################################################

def _i2c_master_set_slave_address_in_slpgbt(lpgbtInst, master_id, slave_address, slpgbt_device_add, slpgbt_master_id):
        """Set I2C master slave address

        Arguments:
            master_id: lpGBT I2C master to use
            slave_address: I2C slave bus address
            addr_10bit: enable 10-bit addressing format
        """
        assert slave_address in range(128), "Unsupported I2C slave address"

        offset_wr, _ = lpgbtInst._i2c_master_get_reg_offsets(slpgbt_master_id)

        address_low = slave_address & 0x7F
        address_high = (slave_address >> 7) & 0x07
        i2c_master_write(
            lpgbtInst,
            master_id,
            slpgbt_device_add,
            address_width=2,
            reg_address = lpgbtInst.I2CM0ADDRESS.address + offset_wr,
            new_reg_val = address_low
        )

def _i2c_master_issue_command_in_slpgbt(lpgbtInst, master_id, command, slpgbt_device_add, slpgbt_master_id):
        """Execute I2C master read/write command

        Arguments:
            master_id: lpGBT I2C master to use
            command: command to be executed
            addr_10bit: execute 10 bit addressing version of command (R/W only)
        """
        assert master_id in range(3), "Invalid I2C master ID"

        offset_wr, _ = lpgbtInst._i2c_master_get_reg_offsets(slpgbt_master_id)

        i2c_master_write(
          lpgbtInst,
          master_id,
          slpgbt_device_add,
          address_width=2,
          reg_address = lpgbtInst.I2CM0CMD.address + offset_wr,
          new_reg_val = command
        )

def _i2c_master_set_nbytes_in_slpgbt(lpgbtInst, master_id, nbytes, slpgbt_device_add, slpgbt_master_id):
        """
        Configures number of bytes used during multi-byte READ and WRITE
        transactions. Implemented as a RMW operation in order to preserve
        I2C master configuration fields.

        Arguments:
            master_id: lpGBT I2C master to use
            nbytes: Number of transaction data bytes
        """
        assert master_id in range(3), "Invalid I2C master ID"
        assert nbytes in range(16), "Invalid transaction length"

        offset_wr, offset_rd = lpgbtInst._i2c_master_get_reg_offsets(slpgbt_master_id)

        control_reg_val = i2c_master_read(
            lpgbtInst,
            master_id,
            slpgbt_device_add,
            read_len = 1,
            reg_address_width = 2,
            reg_address = lpgbtInst.I2CM0CTRL.address + offset_rd
        )
        control_reg_val &= (
            ~lpgbtInst.I2cmConfigReg.NBYTES.bit_mask
        )  # pylint: disable=invalid-unary-operand-type
        
        i2c_master_write(
          lpgbtInst,
          master_id,
          slpgbt_device_add,
          address_width=2,
          reg_address = lpgbtInst.I2CM0DATA0.address + offset_wr,
          new_reg_val = control_reg_val | nbytes << lpgbtInst.I2cmConfigReg.NBYTES.offset
        )
        
        _i2c_master_issue_command_in_slpgbt(lpgbtInst, master_id, lpgbtInst.I2cmCommand.WRITE_CRA, slpgbt_device_add, slpgbt_master_id)

def _i2c_master_await_completion_in_slpgbt(lpgbtInst, master_id, slpgbt_device_add, slpgbt_master_id, timeout=0.1):
        """Wait until I2C master transaction is finished

        Arguments:
            master_id: lpGBT I2C master to use
            timeout: I2C write completion timeout

        Raises:
            LpgbtException: if timeout is exceeded
                            if the transaction is not acknowledged by the slave
                            if the SDA line is pulled down
        """
        _, offset_rd = lpgbtInst._i2c_master_get_reg_offsets(slpgbt_master_id)

        timeout_time = time.time() + timeout
        while True:
            status = i2c_master_read(
              lpgbtInst,
              master_id,
              slpgbt_device_add,
              read_len = 1,
              reg_address_width = 2,
              reg_address = lpgbtInst.I2CM0STATUS.address + offset_rd
            )
            if status & lpgbtInst.I2cmStatusReg.SUCC.bit_mask:
                break

            if status & lpgbtInst.I2cmStatusReg.LEVEERR.bit_mask:
                raise Exception(
                    "The SDA line is pulled low before initiating a transaction."
                )

            if status & lpgbtInst.I2cmStatusReg.NOACK.bit_mask:
                raise Exception(
                    "The last transaction was not acknowledged by the I2C slave"
                )

            if time.time() > timeout_time:
                raise Exception(
                    f"Timeout while waiting for I2C master to finish (status:0x{status:02x})"
                )


def i2c_slave_read(
        lpgbtInst,
        master_id,
        slave_address,
        read_len,
        reg_address_width,
        reg_address,
        slpgbt_device_add,
        slpgbt_master_id,
        timeout=0.1,
        addr_10bit=False
):
        """
        Performs a register-based read using an lpGBT I2C master.
        Register address pointer is written to the slave using a write
        transaction. Then, a multi-byte read transction is triggered to read
        slave register contents.

        Arguments:
            master_id: lpGBT I2C master to use
            slave_address: I2C slave bus address
            read_len: number of bytes to read from slave
            reg_address_width: length of register address in bytes
            reg_address: slave register read address
            timeout: I2C write completion timeout
            addr_10bit: enable 10-bit addressing format
        """
        # pylint: disable=too-many-arguments
        assert master_id in range(3), "Invalid I2C master ID"
        assert read_len in range(17), "Unsupported I2C read length"
        assert reg_address_width in range(5), "Invalid register address width"

        offset_wr, offset_rd = lpgbtInst._i2c_master_get_reg_offsets(master_id)
        slpgbt_offset_wr, slpgbt_offset_rd = lpgbtInst._i2c_master_get_reg_offsets(slpgbt_master_id)

        _i2c_master_set_slave_address_in_slpgbt(lpgbtInst, master_id, slave_address, slpgbt_device_add, slpgbt_master_id)

        for i in range(reg_address_width):
            i2c_master_write(
                lpgbtInst,
                master_id,
                slpgbt_device_add,
                address_width=2,
                reg_address = lpgbtInst.I2CM0DATA0.address + slpgbt_offset_wr + i,
                new_reg_val = (reg_address >> (8 * i)) & 0xFF
            )

        _i2c_master_issue_command_in_slpgbt(lpgbtInst, master_id, lpgbtInst.I2cmCommand.W_MULTI_4BYTE0, slpgbt_device_add, slpgbt_master_id)

        _i2c_master_set_nbytes_in_slpgbt(lpgbtInst, master_id, reg_address_width, slpgbt_device_add, slpgbt_master_id)
        
        _i2c_master_issue_command_in_slpgbt(lpgbtInst, master_id, lpgbtInst.I2cmCommand.WRITE_MULTI, slpgbt_device_add, slpgbt_master_id)
        
        _i2c_master_await_completion_in_slpgbt(lpgbtInst, master_id, slpgbt_device_add, slpgbt_master_id, timeout)

        _i2c_master_set_nbytes_in_slpgbt(lpgbtInst, master_id, read_len, slpgbt_device_add, slpgbt_master_id)
        
        _i2c_master_issue_command_in_slpgbt(lpgbtInst, master_id, lpgbtInst.I2cmCommand.READ_MULTI, slpgbt_device_add, slpgbt_master_id)
        
        _i2c_master_await_completion_in_slpgbt(lpgbtInst, master_id, slpgbt_device_add, slpgbt_master_id, timeout)

        read_values = []
        for i in range(read_len):
            single_byte = i2c_master_read(
                lpgbtInst, 
                master_id,
                slave_address = slpgbt_device_add,
                read_len = 1,
                reg_address_width = 2,
                reg_address = lpgbtInst.I2CM0READ15.address + slpgbt_offset_rd - i,
            )
            
            read_values.append(single_byte)
        if len(read_values) == 1:
            return read_values[0]
        return read_values


def i2c_slave_write(
        lpgbtInst,
        master_id,
        slave_address,
        reg_address_width,
        reg_address,
        data,
        slpgbt_device_add,
        slpgbt_master_id,
        timeout=0.1,
        addr_10bit=False,
):
        """
        Performs a register-based write using an lpGBT I2C master.
        Register address is placed LSbyte first in the transaction data buffer,
        followed by the register contents (variable length).

        Arguments:
            master_id: lpGBT I2C master to use
            slave_address: I2C slave bus address
            reg_address_width: length of register address in bytes
            reg_address: slave register write address
            data: data to write (single byte or list of bytes)
            timeout: I2C write completion timeout
            addr_10bit: enable 10-bit addressing format
        """
        # pylint: disable=too-many-arguments
        assert master_id in range(3), "Invalid I2C master ID"
        assert slpgbt_master_id in range(3), "Invalid I2C master ID"

        offset_wr, _ = lpgbtInst._i2c_master_get_reg_offsets(master_id)
        slpgbt_offset_wr, slpgbt_offset_rd = lpgbtInst._i2c_master_get_reg_offsets(slpgbt_master_id)


        address_and_data = []
        for i in range(reg_address_width):
            address_and_data.append((reg_address >> (8 * i)) & 0xFF)
        try:
            data_list = list(data)
        except TypeError:
            data_list = list((data,))
        address_and_data.extend(data_list)
        assert len(address_and_data) in range(16), "Unsupported I2C write length"

        _i2c_master_set_nbytes_in_slpgbt(lpgbtInst, master_id, len(address_and_data), slpgbt_device_add, slpgbt_master_id)

        for i, data_byte in enumerate(address_and_data):
            i2c_master_write(
                lpgbtInst,
                master_id,
                slpgbt_device_add,
                address_width=2,
                reg_address = lpgbtInst.I2CM0DATA0.address + slpgbt_offset_wr + (i % 4),
                new_reg_val = data_byte
            )
            if i % 4 == 3 or i == len(address_and_data) - 1:
                _i2c_master_issue_command_in_slpgbt(lpgbtInst, master_id, lpgbtInst.I2cmCommand.W_MULTI_4BYTE0 + (i // 4), slpgbt_device_add, slpgbt_master_id)
        
        _i2c_master_set_slave_address_in_slpgbt(lpgbtInst, master_id, slave_address, slpgbt_device_add, slpgbt_master_id)

        _i2c_master_issue_command_in_slpgbt(lpgbtInst, master_id, lpgbtInst.I2cmCommand.WRITE_MULTI, slpgbt_device_add, slpgbt_master_id)

        _i2c_master_await_completion_in_slpgbt(lpgbtInst, master_id, slpgbt_device_add, slpgbt_master_id, timeout)


###########################################################################################

##################### E FUSES on slave lpgbts ###################################

def fuses_read_bank(lpgbtInst, slpgbt_address, address, timeout=0.01):
    """Reads a single bank of eFuses

    Arguments:
        address: first register address to read (multiple of 4)
        timeout: timeout for read completion
    """
    if address % 4 != 0:
        raise LpgbtFuseError(
            f"Incorrect address for read bank! (address=0x{address:02x})"
        )
    _, _, address_high, address_low = u32_to_bytes(address)
#    self.write_reg(self.FUSEBLOWADDH, address_high)
    i2c_master_write(lpgbtInst, 2, slpgbt_address, 2, lpgbtInst.decode_reg_addr(lpgbtInst.FUSEBLOWADDH), address_high)
#    self.write_reg(self.FUSEBLOWADDL, address_low)
    i2c_master_write(lpgbtInst, 2, slpgbt_address, 2, lpgbtInst.decode_reg_addr(lpgbtInst.FUSEBLOWADDL), address_low)
#    self.write_reg(self.FUSECONTROL, self.FUSECONTROL.FUSEREAD.bit_mask)
    i2c_master_write(lpgbtInst, 2, slpgbt_address, 2, lpgbtInst.decode_reg_addr(lpgbtInst.FUSECONTROL), lpgbtInst.FUSECONTROL.FUSEREAD.bit_mask)


    timeout_time = time.time() + timeout

    while True:
#        status = self.read_reg(self.FUSESTATUS)
        status = i2c_master_read(lpgbtInst, 2, slpgbt_address, 1, 2, lpgbtInst.decode_reg_addr(lpgbtInst.FUSESTATUS))
        if status & lpgbtInst.FUSESTATUS.FUSEDATAVALID.bit_mask:
#            fuse0, fuse1, fuse2, fuse3 = self.read_regs(self.FUSEVALUESA, 4)
            fuse_list = i2c_master_read(lpgbtInst, 2, slpgbt_address, 4, 2, lpgbtInst.decode_reg_addr(lpgbtInst.FUSEVALUESA))
            if len(fuse_list) == 4:
              fuse0 = fuse_list[0]
              fuse1 = fuse_list[1]
              fuse2 = fuse_list[2]
              fuse3 = fuse_list[3]
            else:
              raise Exception("Wrong len in fuses readout")
            value = fuse0 | fuse1 << 8 | fuse2 << 16 | fuse3 << 24
#            self.write_reg(self.FUSECONTROL, 0)
            i2c_master_write(lpgbtInst, 2, slpgbt_address, 2, lpgbtInst.decode_reg_addr(lpgbtInst.FUSECONTROL), 0)
            break

        if time.time() > timeout_time:
#            self.write_reg(self.FUSECONTROL, 0)
            i2c_master_write(lpgbtInst, 2, slpgbt_address, 2, lpgbtInst.decode_reg_addr(lpgbtInst.FUSECONTROL), 0)
            raise LpgbtTimeoutError("Timeout while waiting for reading fuses")

    return value


###########################################################################################

def get_VTRx_address(lpgbtInst):
    device_addr = 0
    for device_addr in range(1,128):
        try: 
            i2c_master_read(lpgbtInst, 1, device_addr, 1, 1, 0x00)
            break
        except Exception as e: # exception if read_byte fails
            pass

    return device_addr

def get_VTRx_registers(lpgbtInst):
    vtrx_device = get_VTRx_address(lpgbtInst)
    for key in vtrx_reg_map:
      try:
        vtrx_reg_map[key]["val"] = i2c_master_read(lpgbtInst, 1, vtrx_device, 1, 1, key)
      except Exception as ex: # No device found at this address, simply skip
        pass
    return vtrx_reg_map

def write_vtrx_reg(lpgbtInst, vtrx_address, reg_address, write_data):
    i2c_master_write(
        lpgbtInst,
        1,
        vtrx_address,
        1,
        reg_address,
        write_data
    )
